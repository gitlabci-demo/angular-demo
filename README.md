# Continuous Integration of an Angular 2 project with GitLab

[![pipeline status](https://gitlab.com/gitlabci-demo/angular-demo/badges/master/pipeline.svg)](https://gitlab.com/gitlabci-demo/angular-demo/commits/master)

The Angular 2 / Angular CLI example is based on the following free template:
https://www.creative-tim.com/product/material-dashboard-angular2

The code is built and packaged into a Docker container with an NGinX server properly configured to serve the web site.
